package com.sis.sd.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;

import org.hibernate.annotations.DynamicUpdate;

import com.sis.sd.constant.CompanyTypes;
import com.sis.sd.constant.Operates;
import com.sis.sd.constant.ServiceCode;

import lombok.Data;

@Entity
@Data
@DynamicUpdate
public class Company {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "key_store")
	@TableGenerator(name = "key_store",
					pkColumnValue = "company_key",
					allocationSize = 100)
	@Column(name = "c_id")
	private Long id;
	
	@Column(name = "c_name")
	private String name;
	
	@Column(name = "c_reg_id")
	private String comRegId;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "c_type")
	private CompanyTypes comType;
	
	
	@Column(name = "parent_org_name")
	private String parentOrg;
	
	@Enumerated(EnumType.STRING)
	@Column(name="c_mode_of_operate")
	private Operates modeOfOperate;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "c_service_code")
	private ServiceCode serviceCode;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_fk_id")
	private Address address;

}
