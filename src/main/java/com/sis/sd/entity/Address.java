package com.sis.sd.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

import lombok.Data;

@Entity
@Data
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "key_store")
	@TableGenerator(name = "key_store",
				allocationSize = 100,
				pkColumnValue = "address_key")
	@Column(name = "a_id")
	private Long id;
	
	@Column(name="a_line1")
	private String line1;
	
	@Column(name = "a_line2")
	private String line2;
	
	@Column(name = "a_city")
	private String city;
	
	@Column(name = "a_state")
	private String state;
	
	@Column(name = "a_country")
	private String country;
	
	@Column(name="a_pincode")
	private Long pincode;

}

