package com.sis.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sis.sd.entity.Company;
import com.sis.sd.service.CompanyService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/company/")
@Slf4j
public class CompanyRestController {
	
	@Autowired
	private CompanyService service;
	
	@PostMapping("save")
	public ResponseEntity<String> createCompany(@RequestBody Company company){
		
		ResponseEntity<String> response = null;
		log.info("ENTER INTO SAVE METHOD");
		try {
			Long id = service.createCompany(company);
			log.info("COMPANY IS CREATED {}.", id);
			response = ResponseEntity.ok("Company created with id: "+id);
			
		}catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		log.info("ABOUT TO LEAVE SAVE METHOD");
		
		return response;
		
	}
	
	@PutMapping("/update")
	public ResponseEntity<String> updateCompany(@RequestBody Company company){
		
		ResponseEntity<String> response = null;
		log.info("ENTER INTO UPDATE METHOD");
		try {
			service.updateCompany(company);
			response = ResponseEntity.ok("Company updated succesfully...");
			log.info("COMPANY UPDATED SUCCESSFULLY");
		}catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		log.info("ABOUT TO LEAVE UPDATE METHOD");
		
		return response;
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Company>> getAllCompany(){
		List<Company> companies = service.getAllCompany();
		return ResponseEntity.ok(companies);
	}
	
	@GetMapping("fetch/{id}")
	public ResponseEntity<Company> getOneCompany(@PathVariable Long id){
		
		ResponseEntity<Company> response = null;
		
		log.info("ENTER INTO FETCH METHOD");
		
		try {
			Company company = service.getOneCompany(id);
			response = ResponseEntity.ok(company);
			log.info("COMPANY FETCHED SUCCESSFULLY");
		}catch (Exception e) {
			throw e;
		}
		
		return response;
	}

}
