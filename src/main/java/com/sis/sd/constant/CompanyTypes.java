package com.sis.sd.constant;

public enum CompanyTypes {
	
	IT, FINANCE, BANKING, GOVT, TRANSPORT
}
