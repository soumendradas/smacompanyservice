package com.sis.sd.exceptions;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.sis.sd.payload.ErrorMessage;

@RestControllerAdvice
public class CustomGlobalExceptionHandler {
	
	@ExceptionHandler(CompanyNotFoundException.class)
	public ResponseEntity<ErrorMessage> handleCompanyNotFoundException(CompanyNotFoundException e){
		
		return ResponseEntity.internalServerError()
				.body(new ErrorMessage(
						new Date().toString(),
						e.getMessage(),
						HttpStatus.INTERNAL_SERVER_ERROR.value(),
						HttpStatus.INTERNAL_SERVER_ERROR.toString()));
	}

}
