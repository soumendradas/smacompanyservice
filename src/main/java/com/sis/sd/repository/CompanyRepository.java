package com.sis.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sis.sd.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Long>{

}
