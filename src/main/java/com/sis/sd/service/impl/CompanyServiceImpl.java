package com.sis.sd.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sis.sd.entity.Company;
import com.sis.sd.exceptions.CompanyNotFoundException;
import com.sis.sd.repository.CompanyRepository;
import com.sis.sd.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService{
	
	@Autowired
	private CompanyRepository repo;
	
	public Long createCompany(Company company) throws CompanyNotFoundException {
		
		return repo.save(company).getId();
	}

	
	public void updateCompany(Company company) {

		if(repo.existsById(company.getId()) && company.getId()!=null) {
			repo.save(company);
		}else {
			throw new CompanyNotFoundException("Company not found");
		}
	}

	
	public Company getOneCompany(Long id) {
		
		return repo.findById(id).orElseThrow(()-> new CompanyNotFoundException("Id is invalid"));
	}

	
	public List<Company> getAllCompany() {
		
		return repo.findAll();
	}

}
