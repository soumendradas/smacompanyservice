package com.sis.sd.service;

import java.util.List;

import com.sis.sd.entity.Company;

public interface CompanyService {

	Long createCompany(Company company);
	void updateCompany(Company company);
	Company getOneCompany(Long id);
	List<Company> getAllCompany();
	
}
